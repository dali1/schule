from decimal import Decimal #rundungsfehler vermeiden
import locale

locale.setlocale(locale.LC_ALL, 'de_DE') #für währungsformat setzen

print("Darlehen Rechner für Endfalliges Darlehen")

kreditbetrag_dec = Decimal(input("Kreditbetrag (€):"))
zinssatz_dec = Decimal(input("Zinssatz (%):"))
jahre_dec = Decimal(input("Jahre:"))

zinssatznum_dec = Decimal(zinssatz_dec / 100)

#berechnung
endfaelligwert_dec = Decimal(kreditbetrag_dec * zinssatznum_dec * jahre_dec)

#komischerweise steht vor dem Betrag "Eu", habe das jetzt durch replace entfernt. Geht sich eleganter...
print("\n------Ergebnis------")
print("Zinskosten:" + locale.currency(endfaelligwert_dec, grouping=True).replace("Eu","") + "€")
print("Gesamkosten:" + locale.currency(endfaelligwert_dec + kreditbetrag_dec, grouping=True).replace("Eu","") + "€\n\n")

print("üäöß") #bei mir gehen umlaute standarmäßig schon

print("Fertig!")

